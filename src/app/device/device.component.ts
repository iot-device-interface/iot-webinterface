import { Component, OnInit } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { Observable, throwError } from 'rxjs';
import { map, catchError} from 'rxjs/operators';
import {FormControl, FormGroup} from '@angular/forms';
import {AppComponent} from '../app.component';

@Component({
  selector: 'app-device',
  templateUrl: './device.component.html',
  styleUrls: ['./device.component.css']
})
export class DeviceComponent implements OnInit {

  public generatedKey: any = 'Generierter Key steht hier:';

  public deviceList;
  public registryList;

  registryForm = new FormGroup({
    deviceName: new FormControl('')
  });

  constructor(private http: HttpClient, private app: AppComponent) { }

  ngOnInit(): void {
    this.reloadData();
  }

  reloadData(): void {
    this.http.get('http://localhost:8080/api/v1/devices/list').pipe(catchError(this.handleListDevicesError)).subscribe(
      data => {
        this.deviceList = data; }
    );

    this.http.get('http://localhost:8080/api/v1/registry/list').pipe(catchError(this.handleListRegistriesError)).subscribe(
      data => {
        this.registryList = data; }
    );
  }

  generateKey(): void {
    if (!this.registryForm.get('deviceName').value) {
      Swal.fire({
        icon: 'error',
        title: 'Fehler',
        text: 'Bitte fülle alle Felder aus.',
      });
    }else {
      console.log('test');
      this.http.post('http://localhost:8080/api/v1/registry/add', { name: this.registryForm.get('deviceName').value }).pipe(catchError(this.handleRegistryAddError)).subscribe(
        data => {
          Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Der Key wurde registriert!',
            showConfirmButton: false,
            timer: 1500
          });
          this.generatedKey = data;
          this.generatedKey = this.generatedKey.authenticationKey;
          this.registryList.push(data);
          this.registryForm.reset();
        });
    }
  }

  deleteRegistry(name): void {
    this.http.delete('http://localhost:8080/api/v1/registry/remove?name=' + name).subscribe(res => {
      let reg;
      // tslint:disable-next-line:only-arrow-functions
      this.registryList.forEach( function(registry): any {
        if (registry.name === name) {
          reg = registry;
        }
      });

      this.registryList = this.registryList.filter(obj => obj !== reg);
    });
  }

  handleListDevicesError(error): any {
    Swal.fire({
      position: 'top-end',
      icon: 'error',
      title: 'Es gab einen Fehler beim Auflisten der Benutzer.',
      showConfirmButton: false,
      timer: 3000
    });

    return throwError(error);
  }

  handleListRegistriesError(error): any {
    Swal.fire({
      position: 'top-end',
      icon: 'error',
      title: 'Es gab ein Fehler beim Auflisten der Registries.',
      showConfirmButton: false,
      timer: 3000
    });

    return throwError(error);
  }


  handleRegistryAddError(error): any {
    if (error.status === 500) {
      Swal.fire({
        position: 'top-end',
        icon: 'error',
        title: 'Dieser Gerätename existiert bereits!',
        showConfirmButton: false,
        timer: 1500
      });
    }else {
      Swal.fire({
        position: 'top-end',
        icon: 'error',
        title: 'Ein unerwarteter Fehler ist aufegetreten.',
        showConfirmButton: false,
        timer: 3000
      });
    }
    return throwError(error);
  }

}
