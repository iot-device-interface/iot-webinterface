import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {DeviceComponent} from './device/device.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {UserComponent} from './user/user.component';
import {AuthGuardService} from './auth/auth-guard.service';
import {SessionsComponent} from './sessions/sessions.component';
import {FileComponent} from './file/file.component';

const routes: Routes = [
  {path: 'device', component: DeviceComponent, canActivate: [AuthGuardService]},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'user', component: UserComponent, canActivate: [AuthGuardService]},
  {path: 'sessions', component: SessionsComponent, canActivate: [AuthGuardService]},
  {path: 'files', component: FileComponent, canActivate: [AuthGuardService]},
  {path: '**', redirectTo: 'login'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
