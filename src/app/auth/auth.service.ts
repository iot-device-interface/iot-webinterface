import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse, HttpErrorResponse} from '@angular/common/http';
import {Router} from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {catchError, delay} from 'rxjs/operators';
import {throwError} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  sessionData;
  sessionsList;

  temporaryLookupSID;

  constructor(private http: HttpClient, private router: Router) {}

  auth(login: string, pw: string): void {
    const body = {
      username: login,
      password: pw
    };

    this.http.post('http://localhost:8080/api/v1/session', body).pipe(catchError(this.handleLoginError)).subscribe(
      data => {
        this.sessionData = data;
        localStorage.setItem('sid', this.sessionData.sessionId);
        this.router.navigate(['/device']);
      }
    );
  }

  deleteSession(sessionid: string, inSessionSite: boolean): void {
    this.http.delete('http://localhost:8080/api/v1/session/' + sessionid).subscribe(data => {
      if (sessionid === localStorage.getItem('sid')) {
        this.removeSessionId();
        this.router.navigate(['/login']);
        localStorage.setItem('e', '1');
      }else if (inSessionSite) {
        let ses;
        // tslint:disable-next-line:only-arrow-functions
        this.sessionsList.forEach( function(session): any {
          if (session.sessionId === sessionid) {
            ses = session;
          }
        });

        this.sessionsList = this.sessionsList.filter(obj => obj !== ses);

      }
    });
  }

  getSessionByUserId(userid): any {
    this.http.get('http://localhost:8080/api/v1/session/userid/' + userid).subscribe(data => {
      if (data !== undefined) {
        delay(200);
        const session = data;
        return session;
      }else {
        console.log('testo');
      }
    });
  }

  isAuthenticated(): boolean {
    return this.getSessionId() !== undefined && this.getSessionId() !== null;
  }

  getSessionId(): string {
    return localStorage.getItem('sid');
  }

  removeSessionId(): void {
    localStorage.removeItem('sid');
  }

  // tslint:disable-next-line:typedef
  handleLoginError(error: any)  {
    if (error.status === 404) {
      Swal.fire({
        icon: 'error',
        title: 'Fehler',
        text: 'Dieser Benutzer konnte nicht gefunden werden',
      });
    }else if (error.status === 400) {
      Swal.fire({
        icon: 'error',
        title: 'Fehler',
        text: 'Bitte überprüfe dein Passwort!',
      });
    }else {
      Swal.fire({
        icon: 'error',
        title: 'Fehler',
        text: 'Ein unerwarteter Fehler ist aufgetreten.',
      });
    }

    return throwError(error);
  }
}
