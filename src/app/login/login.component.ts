import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AuthService} from '../auth/auth.service';
import {FormControl, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm = new FormGroup({
    username: new FormControl(''),
    password: new FormControl('')
  });

  private username: any;
  private password: any;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    if (this.authService.isAuthenticated()) {
      this.router.navigate(['/device']);
    }

    if (localStorage.getItem('e') !== null) {
      localStorage.removeItem('e');
      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Logout war erfolgreich',
        showConfirmButton: false,
        timer: 2500
      });
    }
  }

  login(): void {
    if (!this.loginForm.get('password') || !this.loginForm.get('username').value) {
      Swal.fire({
        icon: 'error',
        title: 'Fehler',
        text: 'Bitte fülle alle Felder aus.',
      });
    }else {
      this.username = this.loginForm.get('username').value;
      this.password = this.loginForm.get('password').value;

      this.authService.auth(this.username, this.password);
    }

  }

}
