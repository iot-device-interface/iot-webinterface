import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {FormControl, FormGroup} from '@angular/forms';
import {catchError, delay, timeout} from 'rxjs/operators';
import {throwError} from 'rxjs';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import {AppComponent} from '../app.component';
import {AuthService} from '../auth/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']

})

export class UserComponent implements OnInit {

  userList;
  filteredList;

  userSearch;

  activeSearch = false;

  user = ({
    userid: '',
    username: '',
    email: '',
    password: '',
    activeSession: '',
    sessionId: '',
    createdAt: '',
    lastLogin: '',
    sessionStart: '',
    sessionEnd: ''
  });

  // tslint:disable-next-line:max-line-length
  constructor(private http: HttpClient, private app: AppComponent, public authService: AuthService, private route: ActivatedRoute, private router: Router, private modalService: NgbModal) { }

  userFrom = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
    email: new FormControl(''),
  });

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      this.userSearch = params.q;

      if (params.q === undefined) {
        this.reloadData();
      }else {
        this.activeSearch = true;
        this.userLookUp();
      }
    });

  }


  reloadData(): void {
    this.http.get('http://localhost:8080/api/v1/user/list').pipe(timeout(3000)).subscribe(
      data => {
        this.userList = data; }
    );
  }

  createUser(): void {
    const body = {
      username: this.userFrom.get('username').value,
      password: this.userFrom.get('password').value,
      email: this.userFrom.get('email').value
    };

    if (!body.username || !body.password || !body.email) {
      Swal.fire({
        icon: 'error',
        title: 'Fehler',
        text: 'Bitte fülle alle Felder aus!',
      });
    }else {
      // tslint:disable-next-line:max-line-length
      this.http.post('http://localhost:8080/api/v1/user/', body, {responseType: 'text'}).pipe(catchError(this.handleErrorCreateUser)).subscribe(data => {
        Swal.fire({
          position: 'top-end',
          icon: 'success',
          title: 'Der Benutzer wurde erstellt!',
          showConfirmButton: false,
          timer: 1500
        });

        this.userFrom.reset();
        this.userList.push(JSON.parse(data));
      });
    }
  }

  // tslint:disable-next-line:typedef
  handleErrorCreateUser(error) {
    Swal.fire({
      icon: 'error',
      title: 'Fehler',
      text: 'Ein Benutzer mit diesem Namen oder mit dieser Email exestiert bereits!',
    });
    this.userFrom.reset();
    return throwError(error);
  }

  // tslint:disable-next-line:typedef
  handleErrorUserLookUp(error) {
    Swal.fire({
      icon: 'error',
      title: 'Fehler',
      text: 'Dieser Benutzer konnte nicht gefunden werden',
    });

    return throwError(error);
  }

  userLookUp(): void {
    if (this.activeSearch) {

      if (this.userList === undefined) {

        this.http.get('http://localhost:8080/api/v1/user/list').pipe(timeout(3000)).subscribe(
          data => {
            this.userList = data;

            const user = this.userList.filter(filteredUser => {
              return filteredUser.username.startsWith(this.userSearch);
            });

            this.filteredList = user;
          });

      }else {
        const user = this.userList.filter(filteredUser => {
          return filteredUser.username.startsWith(this.userSearch);
        });

        this.filteredList = user;
      }
    }

  }

  // tslint:disable-next-line:typedef
  updateQueryParams(event: any) {
    this.userSearch = event.target.value;

    if (this.userSearch === '') {
      this.resetSearch();
      this.activeSearch = false;
      return;
    }

    this.router.navigate([], {queryParams: {q: this.userSearch}});
    this.userLookUp();
  }

  userHasSession(user): boolean {
    return user.activeSession;
  }

  resetSearch(): void {
    if (this.activeSearch) {
      this.router.navigateByUrl('/user');
      this.activeSearch = false;
    }
  }

  openModal(content, userid): void {
    const user = this.userList.filter(filteredUser => {
      return filteredUser.id === userid;
    });

    this.user.userid = userid;
    this.user.username = user[0].username;
    this.user.email = user[0].email;
    this.user.activeSession = user[0].activeSession;
    this.user.createdAt = user[0].createdAt;
    this.user.password = user[0].password;
    if (user[0].lastLogin !== null) {
      this.user.lastLogin = user[0].lastLogin;
    }else {
      this.user.lastLogin = 'Noch nicht eingeloggt.';
    }
    if (this.user.activeSession) {
      this.http.get('http://localhost:8080/api/v1/session/userid/' + userid).subscribe(data => {
        if (data !== undefined) {
          const obj = data;
          // @ts-ignore
          this.user.sessionId = obj.sessionId;
          // @ts-ignore
          this.user.sessionStart = obj.created;
          // @ts-ignore
          this.user.sessionEnd = obj.expires;
        }
      });
    }

    this.modalService.open(content, {size: 'lg'});
  }

  deleteUser(userid): void {
    this.http.delete('http://localhost:8080/api/v1/user/' + userid).pipe(catchError(this.handleDeleteUserError)).subscribe(data => {
      Swal.fire({
        position: 'top-end',
        icon: 'info',
        title: 'Der Benutzer wurde gelöscht!',
        showConfirmButton: false,
        timer: 1500
      });

      let us;
      // tslint:disable-next-line:only-arrow-functions
      this.userList.forEach( function(user): any {
        if (user.id === userid) {
          us = user;
        }
      });

      this.userList = this.userList.filter(obj => obj !== us);

      this.modalService.dismissAll();
    });
  }

  handleDeleteUserError(error): any {
    if (error.status === 404) {
      Swal.fire({
        icon: 'error',
        title: 'Fehler',
        text: 'Dieser Benutzer konnte nicht gefunden werden',
      });
    }else {
      Swal.fire({
        icon: 'error',
        title: 'Fehler',
        text: 'Unbekannter Fehler.',
      });
    }

    throwError(error);
  }

  deleteSession(): void {
    this.modalService.dismissAll();

    if (this.user.activeSession) {
      this.authService.deleteSession(this.user.sessionId, false);

      this.reloadData();

      Swal.fire({
        position: 'top-end',
        icon: 'info',
        title: 'Die Session wurde beendet!',
        showConfirmButton: false,
        timer: 1500
      });
    }else {
      Swal.fire({
        icon: 'error',
        title: 'Fehler',
        text: 'Unbekannter Fehler.',
      });
    }
  }

  saveChanges(username, email, password): void {
    username = username.value;
    email = email.value;
    password = password.value;

    if (this.user.username !== username && username !== '') {
      this.user.username = username;
    }

    if (this.user.email !== email && email !== '') {
      if (!email.includes('@')) {
        this.modalService.dismissAll();
        Swal.fire({
          icon: 'error',
          title: 'Fehler',
          text: 'Invalide Email!',
        });
        return;
      }

      this.user.email = email;
    }

    if (this.user.password !== password && password !== '') {
      this.user.password = password;
    }

    const newUser = ({
      id: this.user.userid,
      username: this.user.username,
      password: this.user.password,
      email: this.user.email,
      lastLogin: this.user.lastLogin,
      activeSession: this.user.activeSession,
      createdAt: this.user.createdAt
    });

    if (this.user.lastLogin === 'Noch nicht eingeloggt.') {
      newUser.lastLogin = null;
    }

    // tslint:disable-next-line:max-line-length
    this.http.put('http://localhost:8080/api/v1/user/update/' + this.user.userid, newUser).pipe(catchError(this.handleUserChangeError)).subscribe(data => {
      this.modalService.dismissAll();

      let us;
      // tslint:disable-next-line:only-arrow-functions
      this.userList.forEach( function(user): any {
        if (user.id === newUser.id) {
          us = user;
        }
      });

      this.userList = this.userList.filter(obj => obj !== us);

      this.userList.push(newUser);

      Swal.fire({
        position: 'top-end',
        icon: 'success',
        title: 'Der Nutzer wurde geändert!',
        showConfirmButton: false,
        timer: 1500
      });
    });
  }

  handleUserChangeError(error): any {
    if (error.status === 404) {
      Swal.fire({
        icon: 'error',
        title: 'Fehler',
        text: 'Der Benutzer konnte nicht gefunden werden.',
      });
    }else if (error.status === 500) {
      Swal.fire({
        icon: 'error',
        title: 'Fehler',
        text: 'Server error',
      });
    }else {
      Swal.fire({
        icon: 'error',
        title: 'Fehler',
        text: 'Unbekannter Fehler.',
      });
    }
  }

}
