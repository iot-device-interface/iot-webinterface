import { Component, OnInit } from '@angular/core';
import {catchError} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {AuthService} from '../auth/auth.service';
import {AppComponent} from '../app.component';
import {interval} from 'rxjs';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-sessions',
  templateUrl: './sessions.component.html',
  styleUrls: ['./sessions.component.css']
})
export class SessionsComponent implements OnInit {

  public sessionObject;

  constructor(private http: HttpClient, private router: Router, public authService: AuthService, private app: AppComponent, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.reloadData();

    interval(1000 * 60).subscribe(x => {
      this.reloadData();
    });
  }

  reloadData(): void {
    this.http.get('http://localhost:8080/api/v1/session/list').subscribe(
      data => {
        this.authService.sessionsList = data;
      }
    );
  }

  openModal(content, sessionId): void {
    this.sessionObject = new SessionObject();

    const session = this.authService.sessionsList.filter(fSession => {
      return fSession.sessionId === sessionId;
    });

    this.sessionObject.userId = session[0].userId;
    this.sessionObject.sessionStart = session[0].created;
    this.sessionObject.sessionEnd = session[0].expires;
    this.sessionObject.sessionId = session[0].sessionId;
    this.sessionObject.userAgent = session[0].userAgent;

    this.modalService.open(content, {size: 'lg'});
  }

  test(inputElement): void {
    inputElement.select();
    document.execCommand('copy');
    inputElement.setSelectionRange(0, 0);
  }

}

export class SessionObject {

  userId;

  sessionId;
  sessionStart;
  sessionExpires;
  sessionIp;
  userAgent;

}
