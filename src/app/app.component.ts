import {Component, OnInit} from '@angular/core';
import {AuthService} from './auth/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'iot-web';

  constructor(private authService: AuthService, private router: Router) {}

  logout(): void {
    this.authService.deleteSession(localStorage.getItem('sid'), false);
  }

  isAuthenticated(): boolean {
    return this.authService.isAuthenticated();
  }

  checkRoute(route): boolean {
    if (this.router.url === route) {
      return true;
    }else {
      return false;
    }
  }

  validate(route): string {
    if (this.checkRoute('/' + route)) {
      return 'active';
    }else {
      return null;
    }
  }
}
